#!/usr/bin/env python
#  -*- coding: utf-8 -*-

#Написано сразу и под комбайнер, если припрет, но несколько статистик, в основном запросных - упадут

import sys

current_key = None
current_itent = None
itent = None

for line in sys.stdin:
    key, value = line.rstrip().split('\t')
    key_spl = key.split("@ @")
    itent = key_spl[0]
    value = value.split(" ")

    if current_key == key:
        # Продолжение обработки старого элемента
        if itent in ("QQ", "QD", "DD", "QH", "HH"):
            print ("DANGER DANGER DANGER" + line)

        elif itent in ("QS", "QN"):
            answer = [int(x) + y for x, y in zip(value[:7], answer)]
        elif itent in ("QDS", "QDN"):
            answer = [int(x) + y for x, y in zip(value[:18], answer)]
        elif itent in ("QHS", "QHN"):
            answer = [int(x) + y for x, y in zip(value[:4], answer)]
        else:
            print ("DANGER DANGER DANGER2" + line)

    else:
        if current_key: # Закончить старый результата
            if current_itent in ("QQ", "QS", "QN"):
                imp = answer[0]
                if imp == 0:
                    print ("ERROR ERROR6")
                if answer[1] == 0:
                    answer[1] = -1
                if current_itent in ("QS", "QN"):
                    print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp, answer[5]/imp, answer[6]/imp, answer[6]/answer[1]] + [str(counter)])))
                else:
                    print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp, answer[5]/imp, answer[6]/imp, answer[6]/answer[1]])))

            elif current_itent in ("QD", "QDS", "QDN"):
                imp = answer[0]
                if imp == 0:
                    print ("ERROR ERROR1")
                if answer[5] == 0:
                    if answer[7] != 0:
                        print ("ERROR ERROR15")
                    answer[5] = -1
                if answer[2] == 0:
                    answer[2] = -1
                if answer[6] == 0:
                    answer[6] = -1
                if answer[10] + answer[11] == 0:
                    answer[10] = -1
                    answer[11] = 0
                if answer[3] == 0:
                    answer[3] = -1

                if current_itent in ("QDS", "QDN"):
                    print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                         answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                         answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                         answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                         answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                         answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                         answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                         answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6], answer[17]/answer[5]] + [counter])))

                else:
                    print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                         answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                         answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                         answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                         answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                         answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                         answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                         answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6], answer[17]/answer[5]])))

            elif current_itent in ("QH", "QHS", "QHN"):
                imp = answer[0]
                if imp == 0:
                    print("ERROR ERROR 10")

                if answer[2] == 0:
                    answer[2] = -1
                if current_itent in ("QHS", "QHN"):
                    print (current_key + "\t" + " ".join((str(a) for a in answer +
                    [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[3]/answer[2]] +
                    [counter])))
                else:
                    print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[3]/answer[2]])))

            elif current_itent == "DD":
                imp = answer[0]
                if imp == 0:
                    print("ERROR ERROR 7")
                if answer[5] == 0:
                    if answer[7] != 0:
                        print("ERROR ERROR 17")
                    answer[5] = -1
                if answer[2] == 0:
                    answer[2] = -1
                if answer[6] == 0:
                    answer[6] = -1
                if answer[10] + answer[11] == 0:
                    answer[10] = -1
                    answer[11] = 0
                if answer[3] == 0:
                    answer[3] = -1

                print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                         answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                         answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                         answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                         answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                         answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                         answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                         answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6]])))

            elif current_itent == "HH":
                imp = answer[0]
                if imp == 0:
                    print("ERROR ERROR 4")
                if answer[5] == 0:
                    if answer[7] != 0:
                        print("ERROR ERROR 5")
                    answer[5] = -1
                if answer[2] == 0:
                    answer[2] = -1
                if answer[6] == 0:
                    answer[6] = -1
                if answer[10] + answer[11] == 0:
                    answer[10] = -1
                    answer[11] = 0
                if answer[3] == 0:
                    answer[3] = -1
                print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                         answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                         answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                         answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                         answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                         answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                         answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                         answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6]])))
            else:
                print ("ERROR ERROR 2")

        if itent in ("QQ", "QS", "QN"):
            answer = [int(x) for x in value[:7]]
        elif itent in ("QD", "QDS", "QDN"):
            answer = [int(x) for x in value[:18]]
        elif itent in ("QH", "QHS", "QHN"):
            answer = [int(x) for x in value[:4]]
        elif itent in ("DD"):
            answer = [int(x) for x in value[:17]]
        elif itent in ("HH"):
            answer = [int(x) for x in value[:17]]
        else:
            print("ERROR ERROR1")

        counter = 1
        current_key = key
        current_itent = itent

if current_key: # Закончить последний результата
    if current_itent in ("QQ", "QS", "QN"):
        imp = answer[0]
        if imp == 0:
            print ("ERROR ERROR6")
        if answer[1] == 0:
            answer[1] = -1
        if current_itent in ("QS", "QN"):
            print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp, answer[5]/imp, answer[6]/imp, answer[6]/answer[1]] + [str(counter)])))
        else:
            print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp, answer[5]/imp, answer[6]/imp, answer[6]/answer[1]])))

    elif current_itent in ("QD", "QDS", "QDN"):
        imp = answer[0]
        if imp == 0:
            print ("ERROR ERROR1")
        if answer[5] == 0:
            if answer[7] != 0:
                print ("ERROR ERROR15")
            answer[5] = -1
        if answer[2] == 0:
            answer[2] = -1
        if answer[6] == 0:
            answer[6] = -1
        if answer[10] + answer[11] == 0:
            answer[10] = -1
            answer[11] = 0
        if answer[3] == 0:
            answer[3] = -1

        if current_itent in ("QDS", "QDN"):
            print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                 answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                 answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                 answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                 answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                 answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                 answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                 answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6], answer[17]/answer[5]] + [counter])))

        else:
            print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                 answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                 answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                 answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                 answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                 answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                 answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                 answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6], answer[17]/answer[5]])))

    elif current_itent in ("QH", "QHS", "QHN"):
        imp = answer[0]
        if imp == 0:
            print("ERROR ERROR 10")

        if answer[2] == 0:
            answer[2] = -1
        if current_itent in ("QHS", "QHN"):
            print (current_key + "\t" + " ".join((str(a) for a in answer +
            [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[3]/answer[2]] +
            [counter])))
        else:
            print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[3]/answer[2]])))

    elif current_itent == "DD":
        imp = answer[0]
        if imp == 0:
            print("ERROR ERROR 7")
        if answer[5] == 0:
            if answer[7] != 0:
                print("ERROR ERROR 17")
            answer[5] = -1
        if answer[2] == 0:
            answer[2] = -1
        if answer[6] == 0:
            answer[6] = -1
        if answer[10] + answer[11] == 0:
            answer[10] = -1
            answer[11] = 0
        if answer[3] == 0:
            answer[3] = -1

        print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                 answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                 answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                 answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                 answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                 answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                 answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                 answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6]])))

    elif current_itent == "HH":
        imp = answer[0]
        if imp == 0:
            print("ERROR ERROR 4")
        if answer[5] == 0:
            if answer[7] != 0:
                print("ERROR ERROR 5")
            answer[5] = -1
        if answer[2] == 0:
            answer[2] = -1
        if answer[6] == 0:
            answer[6] = -1
        if answer[10] + answer[11] == 0:
            answer[10] = -1
            answer[11] = 0
        if answer[3] == 0:
            answer[3] = -1
        print (current_key + "\t" + " ".join((str(a) for a in answer + [answer[1]/imp, answer[2]/imp, answer[3]/imp, answer[4]/imp,
                 answer[5]/imp, answer[6]/imp, answer[7]/answer[5], answer[8]/imp,
                 answer[8]/answer[5], answer[8]/answer[2], answer[9]/imp,
                 answer[9]/answer[6], answer[10]/(answer[10] + answer[11]), answer[12]/imp,
                 answer[12]/answer[2], answer[2]/answer[3], answer[13]/imp, answer[14]/imp,
                 answer[15]/imp, answer[5]/answer[2], answer[6]/answer[2],
                 answer[7]/answer[2], answer[9]/answer[2], answer[10]/answer[2],
                 answer[13]/answer[2], answer[14]/answer[2], answer[15]/answer[2], answer[14]/answer[3], answer[16]/answer[6]])))
    else:
        print ("ERROR ERROR 2")
