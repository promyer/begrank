#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os

from pymystem3 import Mystem
from string import punctuation

abs_path_mystem_bin = os.path.abspath("mystem")
mystem = Mystem(mystem_bin=abs_path_mystem_bin)

from urllib.parse import urlparse, ParseResult


russian_stopwords = set()
nltk_stopwords = open('stopwords.txt', 'r')
for line in nltk_stopwords:
    russian_stopwords.add(line.strip())

def normalise(query):

    tokens = mystem.lemmatize(query.lower())
    tokens = [token for token in tokens if token not in russian_stopwords and token != " " and token.strip() not in punctuation]
    return " ".join(tokens)


with open('urls.dat', 'r') as urls_file, open('hosts.dat', 'r') as hosts_file, open('queries.dat', 'r') as queries_file, \
     open('queries_spell.dat', 'r') as queries_spell_file, open('queries_norm.dat', 'r') as queries_norm_file:

    urls = set()
    for url in urls_file:
        urls.add(url.rstrip())

    hosts = set()
    for host in hosts_file:
        hosts.add(host.rstrip())

    queries = set()
    for query in queries_file:
        queries.add(query.rstrip())

    queries_spell = dict()
    for line in queries_spell_file:
        good_query, real_query = line.rstrip().split('\t')
        queries_spell[good_query] = real_query

    queries_norm = dict()
    for line in queries_norm_file:
        good_query, real_query = line.rstrip().split('\t')
        queries_norm[good_query] = real_query


    for ln in sys.stdin:
        line = ln.strip('\n')
        itent = line[:2]
        line = line[2:]
        key, value = line.split('\t')

        if itent == 'QQ':

            if key in queries:
                print ("QQ@ @" + line)

            elif key in queries_spell:
                print("QS@ @" + queries_spell[key] + "\t" + value)

            else:
                key = normalise(key)
                if key in queries_norm:
                    print("QN@ @" + queries_norm[key] + "\t" + value)

        elif itent == 'QD':

            query, url = key.split("@ @")
            parsed_url = urlparse(url)
            url = ParseResult('', *parsed_url[1:]).geturl()[2:]

            if url in urls:
                if query in queries:
                    print ('QD@ @' + query + '@ @' + url + '\t' + value.split('@ @')[0])

                elif query in queries_spell:
                    print("QDS@ @" + queries_spell[query] + "@ @" + url + '\t' + value.split('@ @')[0])

                else:
                    query_norm = normalise(query)
                    if query_norm in queries_norm:
                        print("QDN@ @" + queries_norm[query_norm] + "@ @" + url + '\t' + value.split('@ @')[0])

        elif itent == 'DD':

            parsed_url = urlparse(key)
            url = ParseResult('', *parsed_url[1:]).geturl()[2:]

            if url in urls:
                print ('DD@ @' + url + '\t' + value.split('@ @')[0])

        elif itent == 'QH':

            query, host = key.split("@ @")

            if host in hosts:
                if query in queries:
                    print ('QH@ @' + line)

                elif query in queries_spell:
                    print("QHS@ @" + queries_spell[query] + "@ @" + host + '\t' + value)

                else:
                    query_norm = normalise(query)
                    if key in queries_norm:
                        print("QHN@ @" + queries_spell[query_norm] + "@ @" + host + '\t' + value)

        elif itent == 'HH':

            if key in hosts:
                print ('HH@ @' + key + '\t' + value.split('@ @')[0])

        else:
            print("Error: " + line)
