#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import requests

import nltk
nltk.download("stopwords")

from nltk.corpus import stopwords
from pymystem3 import Mystem
from string import punctuation

mystem = Mystem()
russian_stopwords = stopwords.words("russian")

queries = open("queries.tsv", "r")

query_file = open('queries.dat', 'w')
query_spell_file = open('queries_spell.dat', 'w')
query_normalise_file = open('queries_norm.dat', 'w')

query_count = 0
spelled_q = 0
normed = 0

for line in queries:
    query_count += 1
    query = line.rstrip().split('\t')[1]
    query_file.write(query + '\n')

    spellchecked = query
    answer = requests.get('https://speller.yandex.net/services/spellservice.json/checkText?text='
                            + '+'.join(query.split(' ')) + '&options=516').json()
    shift = 0
    for error in answer:
        if error['code'] != 1:
            print("~~~~~~~~~~~~")
            print (answer)
            print (query)
            print("++++++++++++")
        spellchecked = spellchecked[:error['pos'] + shift] + error['s'][0] + spellchecked[error['pos'] + shift + len(error['word']):]
        shift += len(error['s'][0]) - len(error['word'])

    if spellchecked != query and spellchecked != '':
        spelled_q += 1
        query_spell_file.write(spellchecked + '\t' + query + '\n')

    tokens = mystem.lemmatize(spellchecked.lower())
    tokens = [token for token in tokens if token not in russian_stopwords and token != " " and token.strip() not in punctuation]
    normalised = " ".join(tokens)

    if normalised != spellchecked and normalised != '':
        normed += 1
        query_normalise_file.write(normalised + '\t' + query + '\n')

print (query_count, spelled_q, normed)