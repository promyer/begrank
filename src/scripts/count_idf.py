#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import sys
import _pickle
from collections import defaultdict

file = open(sys.argv[1], "r")
idf = defaultdict(int)
counter = 0

for line in file:
    splited = line.strip().split("\t")
    if None in splited or '' in splited or len(splited) != 2:
        continue
    text = line.split("\t")[1]
    for word in text.split(" "):
        idf[word] += 1
        counter += 1

for ww in idf:
    idf[ww] = counter/idf[ww]

_pickle.dump(idf, open(sys.argv[1] + "_idf.cpickle", "wb"))
