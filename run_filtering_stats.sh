#!/usr/bin/env bash

set -e

OUTDIR=/user/mi.morozov/clean_stats_with_norm/
hadoop fs -rm -r -f $OUTDIR

yarn jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-D mapred.reduce.tasks=8 \
-files "src/hadoop_make_features/python/mapper.py,src/hadoop_make_features/python/reducer.py,urls.dat,hosts.dat,queries.dat,queries.dat,queries_norm.dat,queries_spell.dat,mystem,stopwords.txt" \
-archives "environment.tar.gz" \
-input '/user/mi.morozov/stats' \
-output $OUTDIR \
-mapper "environment.tar.gz/bin/python mapper.py" \
-reducer "environment.tar.gz/bin/python reducer.py"

echo job finished, see output in $OUTDIR
